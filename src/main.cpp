#include <iostream>
#include "pixel.hpp"
#include "image.hpp"

using namespace std;

int main() {

	string path;

	cin >> path;

	Image* img = new Image();
	img->open(path);
	img->load();

	cout << "Comentarios:" << endl;

	for(pair<int,string> p : img->getComment()){
		cout << p.first << " - " << p.second << endl;
	}

	cout << "Largura: " << img->getWidth() << endl;
	cout << "Altura: " << img->getHeight() << endl;
	cout << "Valor Máximo: " << img->getMaxValue() << endl;

	delete img;

	return 0;

}