#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>
#include "image.hpp"

using namespace std;

Image::Image(){
	
}

bool
Image::open(string path){

	this->image.open("doc/" + path + ".ppm");

	return this->image.is_open();
}

void
Image::loadHeader(){
	int aux = 0;
	int current_line = 0;
	while(aux < 3)
	{
		string line;

		getline(this->image, line);
		current_line++;
		if(line[0] == '#')
		{
			this->addComment(line, current_line);
		}
		else
		{
			switch(aux++)
			{
				case 0:
					this->setMagicNumber(line);
				break;
				case 1:
					int aux1, aux2;
					if(2 != sscanf(line.c_str(),"%d %d", &aux1, &aux2)){

						this->setWidth(aux1);
						getline(this->image, line);
						sscanf(line.c_str(),"%d", &aux1);
						this->setHeight(aux1);
					} else {
						this->setWidth(aux1);
						this->setHeight(aux2);
					}
				break;
				case 2:
					int aux;
					sscanf(line.c_str(),"%d", &aux);
					this->setMaxValue(aux);
				break;
				default: break;
			}
		}

	}
}

void
Image::loadBody()
{
	string line;
	while(!this->image.eof()){
		getline(this->image, line);
		int r,g,b;
	//cout << sscanf(line.c_str(), "%d %d %d", &r, &g, &b) << endl;
		while(sscanf(line.c_str(), "%d %d %d", &r, &g, &b) == 3){
			Pixel* aux = new Pixel(r,g,b);
			this->addPixel(aux);
			delete aux;
		}
	}
}

void
Image::load()
{
	this->loadHeader();
	this->loadBody();

}

Image::~Image(){  
	this->image.close();
}
void Image::setHeight(int height){ 
	this->height = height;
}
int Image::getHeight(){
	return height;
}
void Image::setWidth(int width){
	this->width = width;
}
int Image::getWidth(){
	return width;
}
void Image::setMaxValue(int max_value){
	this->max_value = max_value;
}
int Image::getMaxValue(){
	return max_value;
}
void Image::setMagicNumber(string magic_number){
	this->magic_number = magic_number;
}
string Image::getMagicNumber(){
	return magic_number;
}

void Image::addComment(string comment, int line){
	pair<int, string> aux;

	aux.first = line;
	aux.second = comment;

	this->comment.push_back(aux);
}

vector<pair<int,string>> 
Image::getComment(){
	return this->comment;
}

void 
Image::addPixel(const Pixel* p){
	this->pixel.push_back(*p);
}


