#include <iostream>

#include "pixel.hpp"

Pixel::Pixel(int r, int g, int b){
	this->setRed(r);
	this->setGreen(g);
	this->setBlue(b);
}

Pixel::~Pixel(){
}

void Pixel::setRed(int r){
	this->r = r;
}
int Pixel::getRed(){
	return r;
}
void Pixel::setGreen(int g){
	this->g = g;
}
int Pixel::getGreen(){
	return g;
}
void Pixel::setBlue(int b){
	this->b = b;
}
int Pixel::getBlue(){
	return b;
}



