#ifndef IMAGE_H
#define IMAGE_H

#include <fstream>
#include <vector>

#include "pixel.hpp"

using namespace std;

class Image {
private:
	ifstream image;
	int height;
	int width;
	int max_value;
	string magic_number;
	vector<pair<int,string>> comment;
	vector<Pixel> pixel;

	void addComment(string comment, int line);
	void loadHeader();
	void loadBody();
public:
	Image();
	~Image();

	bool open(string path);
	void load();

	void setHeight(int height);
	int getHeight();
	void setWidth(int width);
	int getWidth();
	void setMaxValue(int max_value);
	int getMaxValue();
	void setMagicNumber(string magic_number);
	string getMagicNumber();
	vector<pair<int,string>> getComment();
	void addPixel(const Pixel* p);

};
#endif