#ifndef PIXEL_H
#define PIXEL_H

using namespace std;

class Pixel {
private:
	int r;
	int g;
	int b;
public:
	Pixel(int r, int g, int b);
	~Pixel();
	
	int getRed();
	int getGreen();
	int getBlue();

	void setRed(int r);
	void setGreen(int g);
	void setBlue(int b); 
};

#endif